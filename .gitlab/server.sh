# Pull code
cd $HOME/projects/myapp
eval "$(ssh-agent -s)" && ssh-add $HOME/.ssh/gitlab
git checkout main
git pull origin main

# Build and deploy
node ace migration:run
npm install

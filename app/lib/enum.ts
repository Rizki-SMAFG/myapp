export enum ActivityType {
  FIXED_TIME = 'fixed_time',
  NOT_FIXED_TIME = 'not_fixed_time'
}

export enum RoleActivityMember {
  MANAGER = 'manager',
  MEMBER = 'member'
}

export enum TypeLeaves {
  PAID = 'paid',
  UNPAID = 'unpaid'
}

export enum StatusLeaves {
  APROVE = 'aprove',
  REJECTED = 'rejected',
  WAITING = 'waiting'
}

export enum StatusEmployees {
  FULL_TIME = 'FULL_TIME',
  PART_TIME = 'PART_TIME',
}

export enum LeaveStatuses {
  CUTI = 'cuti',
  SAKIT = 'sakit'
}

export enum AssessmentCategory {
  ASPEK_TEKNIS_PEKERJAAN = 'ASPEK TEKNIS PEKERJAAN',
  ASPEK_NON_TEKNIS = 'ASPEK NON TEKNIS',
  ASPEK_KEPRIBADIAN = 'ASPEK KEPRIBADIAN',
  ASPEK_KEPEMIMPINAN = 'ASPEK KEPEMIMPINAN',
}
